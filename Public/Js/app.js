$(document).ready(function(){
   const regexString =  /^[a-zA-Z\é\è\- ]+$/;
   const regexNum = /^[0-9]+$/;
   const regexAlphaNum = /^[a-zA-Z0-9 ]+$/;
   const regexPhone =  /^((\+)33|0|0033)[1-9](\d{2}){4}$/g;
   const regexMail =  /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
   
   var password = $('#password');
   var passwordVerif = $('#passwordVerif');

   var inputString = {
      name : $("#name"),
      firstName : $("#firstName"),
      adress : $("#adress"),
      cp : $("#cp"),
      ville : $("#ville"),
      tel : $("#tel"),
      email : $("#email"),
      login : $("#login")
      
   }
   var errorMessage = {
      name : $("#errorName"),
      firstName : $("#errorFirstName"),
      adress : $("#errorAdress"),
      cp : $("#errorCp"),
      ville : $("#errorVille"),
      tel : $("#errorTel"),
      email : $("#errorEmail"),
      login : $("#errorLogin")
      
   }
   var regex = {
      name : regexString,
      firstName : regexString,
      adress : regexAlphaNum,
      cp : regexNum,
      ville : regexString,
      tel : regexPhone,
      email : regexMail,
      login : regexAlphaNum

   }

   $("#form").submit(function(event){
      event.preventDefault();
      var validForm = true;
      // Vérifie tout les champs
      for(var key in inputString){
         validForm = checkString(validForm, inputString[key], errorMessage[key], regex[key]);
         if(validForm == false){
            formValid(false);
            break;
         }
      }
      validForm = checkPasword(validForm, password, passwordVerif);
      if(validForm == false){
         formValid(false);
      }else{
         formValid(true);
      }

   });


   function checkString(validForm, string , errorString, regex)
   {
      var validation = string.val().match(regex);
      if((string.val().length < 2 || string.val().length > 100) || !validation)
      {
         errorString.css('visibility', 'visible');
         string.css("backgroundColor", "#fba");
         validForm = false;
         return validForm
      }
      else
      {
         errorString.css('visibility', 'hidden');
         string.css("backgroundColor", "");
         validForm = true;
         return validForm;
      }
   }

   function checkPasword(validForm, password, passwordVerif){
      if(password.val() != passwordVerif.val()){
         validForm = false;
         $('#errorPassword').css('visibility', 'visible');
      }else{
         $('#errorPassword').css('visibility', 'hidden');
      }
      return validForm;
   }


   function formValid(valid){
      if(valid){
         alert("Votre formulaire est valide");
         $('.invalid').css('visibility', 'hidden');
      }else{
         $('.invalid').css('visibility', 'visible');
      }
         
   }

}); 